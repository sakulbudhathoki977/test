import React from "react";
import { Root } from "native-base";
import { StyleSheet, View, StatusBar } from "react-native";
import { Provider } from "react-redux";
import moment from "moment-timezone";
import store, { AppWithNavigationState } from "./configureStore";
import { theme } from "./utils";
import { NetworkStatus } from "./components";

moment.tz.setDefault("Asia/Kathmandu");
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
const App = () => (
  <View style={styles.container}>
    <StatusBar backgroundColor="white" barStyle="dark-content" />

    <Provider store={store}>
      <Root>
        <NetworkStatus />
        <AppWithNavigationState />
      </Root>
    </Provider>
  </View>
);

export default App;

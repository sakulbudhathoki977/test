import { createStore, applyMiddleware, compose } from "redux";

import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer
} from "react-navigation-redux-helpers";

import createSagaMiddleware from "redux-saga";
import { connect } from "react-redux";

import AppNavigator from "./routeConfig";
import createReducer from "./reducers";
import rootSaga from "./sagas";

const navReducer = createNavigationReducer(AppNavigator);

const appReducer = createReducer({ nav: navReducer });

const navMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);
const sagaMiddleware = createSagaMiddleware();

const middlewares = [navMiddleware, sagaMiddleware];

const App = reduxifyNavigator(AppNavigator, "root");
const mapStateToProps = state => ({
  state: state.nav
});
export const AppWithNavigationState = connect(mapStateToProps)(App);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : compose;

const store = createStore(
  appReducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares))
);

sagaMiddleware.run(rootSaga);

if (module.hot) {
  module.hot.accept(() => {
    const nextRootReducer = appReducer;
    store.replaceReducer(nextRootReducer);
  });
}
export default store;

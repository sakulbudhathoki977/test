import {createStackNavigator, createSwitchNavigator} from 'react-navigation';
import Login from './containers/Login';
import DashboardTabsCollection from './routes';

const AppStack = DashboardTabsCollection;

const LoginStack = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  }
});

const AppNavigator = createSwitchNavigator(
  {
    App: AppStack,
    Login: LoginStack
  },
  {
    initialRouteName: 'App',
    lazyLoad: true,
    swipeEnabled: false,
    animationEnabled: false,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0
      }
    })
  }
);

export default AppNavigator;

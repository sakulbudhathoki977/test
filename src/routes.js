import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import { Icon } from "native-base";
import { theme } from "./utils";

import Home from "./containers/Dashboard";
import Notification from "./containers/Notification";
import Settings from "./containers/Settings";
const tabBarStyle = {
  height: 50,
  paddingVertical: 7,
  paddingHorizontal: 10,
  backgroundColor: "white"
};

const DashboardTabsCollection = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="home"
            type="MaterialCommunityIcons"
            style={{ color: tintColor, fontSize: 22 }}
          />
        )
      }
    },

    Alerts: {
      screen: Notification
    },
    Profile: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="account"
            type="MaterialCommunityIcons"
            style={{ color: tintColor, fontSize: 22 }}
          />
        )
      }
    },
  },
  {
    lazy: true,
    initialRouteName: "Home",
    showIcon: true,
    tabBarOptions: {
      activeTintColor: "#455CF6",
      inactiveTintColor: "#929292",
      labelStyle: {
        fontSize: 8
      },
      style: tabBarStyle
    }
  }
);

export default DashboardTabsCollection;

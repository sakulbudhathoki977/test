import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  segment: {
    backgroundColor: "transparent",
    paddingHorizontal: 20,
    flexDirection: "row"
  },
  segmentButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#BFBFBF",
    borderLeftWidth: 1.5,
    borderRightWidth: 1.5,
    borderTopWidth: 1.5,
    borderBottomWidth: 1.5,
    borderColor: "#BFBFBF",
    padding: 0
  },
  centerButton: {
    marginHorizontal: 2
  },
  activeSegmentButton: {
    backgroundColor: "#FFFFFF",
    borderColor: "#BFBFBF"
  },
  segmentText: {
    color: "white",
    alignSelf: "center",
    fontWeight: "500"
  },
  activeSegmentText: {
    color: "#BFBFBF"
  },

  firstButton: {
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  lastButton: {
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  }
});

export default styles;

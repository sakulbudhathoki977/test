import React from "react";
import { Segment, Button } from "native-base";
import Text from "../Text";

import styles from "./styles";

const CustomSegment = props => (
  <Segment style={styles.segment}>
    <Button
      onPress={() => props.updateActiveIndex(0)}
      first
      style={[
        styles.segmentButton,
        styles.firstButton,
        props.activeTabIndex === 0 ? styles.activeSegmentButton : null
      ]}
    >
      <Text
        style={[
          styles.segmentText,
          props.activeTabIndex === 0 ? styles.activeSegmentText : null
        ]}
      >
        PAST
      </Text>
    </Button>
    <Button
      onPress={() => props.updateActiveIndex(1)}
      style={[
        styles.segmentButton,
        styles.centerButton,
        props.activeTabIndex === 1 ? styles.activeSegmentButton : null
      ]}
    >
      <Text
        style={[
          styles.segmentText,
          props.activeTabIndex === 1 ? styles.activeSegmentText : null
        ]}
      >
        FUTURE
      </Text>
    </Button>
    <Button
      onPress={() => props.updateActiveIndex(2)}
      last
      style={[
        styles.segmentButton,
        styles.lastButton,
        props.activeTabIndex === 2 ? styles.activeSegmentButton : null
      ]}
    >
      <Text
        style={[
          styles.segmentText,
          props.activeTabIndex === 2 ? styles.activeSegmentText : null
        ]}
      >
        OVERDUE
      </Text>
    </Button>
  </Segment>
);

export default CustomSegment;

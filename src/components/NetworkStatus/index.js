import React, { Component } from "react";
import { View, Text, NetInfo, AppState } from "react-native";
import styles from "./styles";

class NetworkStatus extends Component {
  state = {
    isConnected: true
  };

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    AppState.addEventListener("change", this.handleAppStateChange);
    NetInfo.isConnected.fetch().then(this.handleConnectionChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    AppState.removeEventListener("change", this.handleAppStateChange);
  }

  handleAppStateChange = nextAppState => {
    if (nextAppState === "active") {
      NetInfo.isConnected.fetch().then(this.handleConnectionChange);
    }
  };

  handleConnectionChange = isConnected => {
    this.setState({ isConnected });
  };

  render() {
    if (!this.state.isConnected) {
      return (
        <View style={styles.container}>
          <Text style={styles.message}>No Internet Connection</Text>
        </View>
      );
    }
    return null;
  }
}

export default NetworkStatus;

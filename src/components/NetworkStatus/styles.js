import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#D1001E",
    flexDirection: "row",
    height: 42,
    justifyContent: "center",
    width: "100%"
  },
  message: {
    color: "#fefefe"
  }
});

export default styles;

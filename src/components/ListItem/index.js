import React, { Component } from "react";
import {
  View,
  Image,
  Platform,
  ProgressBarAndroid,
  ProgressViewIOS,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Icon, Button } from "native-base";
import { pick as _pick } from "lodash";
import { Status, Text } from "..";
import { helpers, theme } from "../../utils";
import styles from "./styles";

const { width } = Dimensions.get("window");

class TempListItem extends Component {
  render() {
    let progress;
    if (this.props.isProgress) {
      progress = +(
        helpers.toPercentage(
          parseFloat(this.props.amount) - parseFloat(this.props.display),
          this.props.amount
        ) / 100
      ).toFixed(2);
    }

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={[
          styles.listItemContainer,
          this.props.style,
          this.props.read === "0" && { backgroundColor: "#ECF7FD" }
        ]}
        button
        onPress={() => this.props.onListPress(this.props)}
      >
        <View style={[styles.listItem]}>
          {this.props.imageUrl && (
            <Image
              style={styles.imageIcon}
              source={{ uri: this.props.imageUrl }}
              resizeMode="contain"
            />
          )}
          {this.props.iconType && (
            <View style={styles.iconContainer}>
              <Icon
                style={styles.icon}
                type={this.props.iconType}
                name={this.props.icon}
              />
            </View>
          )}

          <View style={styles.textContainer}>
            <View style={{ flexDirection: "row" }}>
              {this.props.title && (
                <Text
                  numberOfLines={
                    this.props.primaryNoOfLines
                      ? this.props.primaryNoOfLines
                      : 1
                  }
                  style={[
                    styles.title,
                    this.props.read === "0" && styles.unread
                  ]}
                >
                  {this.props.title}
                </Text>
              )}
            </View>
            {this.props.subTitle && (
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "400",
                  color: "#404A52",
                  marginBottom: 4
                }}
              >
                {this.props.subTitle}
              </Text>
            )}
            {this.props.ternaryText && (
              <Text
                style={{
                  fontSize: 14,
                  letterSpacing: 1,
                  fontWeight: "400",
                  color: "#404A52"
                }}
              >
                {this.props.ternaryText}
              </Text>
            )}
          </View>
          {this.props.value && (
            <Text style={styles.rightText}>{this.props.value}</Text>
          )}

          {this.props.isArrowIcon && (
            <Button
              transparent
              style={styles.arrowButton}
              onPress={() => this.props.onListPress(this.props)}
            >
              <Icon
                style={[
                  styles.arrowIcon,
                  {
                    fontSize: this.props.rightIconSize
                      ? this.props.rightIconSize
                      : 24
                  }
                ]}
                type={
                  this.props.rightIconType
                    ? this.props.rightIconType
                    : "MaterialCommunityIcons"
                }
                name={
                  this.props.rightIconName
                    ? this.props.rightIconName
                    : "chevron-right"
                }
              />
            </Button>
          )}
          {this.props.isStatus && (
            <View style={styles.statusContainer}>
              <Status
                title={this.props.schedule_type_title}
                style={styles.status}
              />
            </View>
          )}
        </View>
        {this.props.isProgress &&
          (Platform.OS === "android" ? (
            <ProgressBarAndroid
              style={styles.progressBar}
              styleAttr="Horizontal"
              color={theme.progressColor}
              progress={progress}
              indeterminate={false}
            />
          ) : (
            <ProgressViewIOS
              style={styles.progressBar}
              progress={progress}
              progressViewStyle="default"
              progressTintColor={theme.progressColor}
            />
          ))}
      </TouchableOpacity>
    );
  }
}

export default TempListItem;

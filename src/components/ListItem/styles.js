import { StyleSheet, Dimensions } from "react-native";
import { theme } from "./../../utils";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
  listItemContainer: {
    flexDirection: "column",
    backgroundColor: "white",
    borderBottomWidth: 0,
    justifyContent: "center",
    borderBottomColor: "#E8E8E8",
    borderBottomWidth: 1
  },
  listItem: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 15
  },
  textContainer: {
    flexDirection: "column",
    flexWrap: "wrap"
  },
  title: {
    width: width - 140,
    marginBottom: 4,
    fontSize: 14,
    fontWeight: "400",
    color: "#404A52"
  },
  imageIcon: {
    height: 40,
    width: 40,
    marginRight: 15
  },
  iconContainer: {
    marginRight: 15,
    width: 30,
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    color: "#929292",
    fontSize: 24
  },
  arrowButton: {
    position: "absolute",
    right: -15,
    width: 60,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  arrowIcon: {
    color: "#A2ABB5"
  },
  progressBar: {
    marginTop: 10,
    marginHorizontal: 20,
    transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }]
  },
  unread: {
    fontWeight: "500",
    backgroundColor: "#ECF7FD"
  },
  statusContainer: {
    position: "absolute",
    right: 30,
    width: 98,
    alignItems: "center",
    justifyContent: "center"
  },
  rightText: {
    position: "absolute",
    right: 0
  }
});

export default styles;

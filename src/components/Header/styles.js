import { StyleSheet, Dimensions, Platform } from "react-native";
import { theme } from "./../../utils";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  header: {
    ...theme.defaultBorderBottom
  },
  backButton: {
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  backButtonIcon: {
    color: "#707070",
    fontSize: 24
  },
  heading: {
    fontSize: 16,
    fontWeight: "bold",
    flex: 2
  },
  rightBox: {
    flexDirection: "row"
  },
  icon: {
    fontSize: 18,
    marginLeft: 14,
    ...theme.defaultTextColor
  },
  searchField: {
    paddingHorizontal: 10,
    width: width - 80,
    height: 50
  }
});

export default styles;

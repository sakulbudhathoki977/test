import React, { Fragment } from "react";
import { Platform, View, TextInput, TouchableOpacity } from "react-native";
import { Button, Icon } from "native-base";
import styled from "styled-components";
import Text from "../Text";
import styles from "./styles";
import { theme } from "./../../utils";

const StyledHeader = styled.View`
  background-color: white;
  flex-direction: row;
  box-shadow: ${props =>
    props.noEffect ? "0px 0px rgba(0,0,0,0)" : "0px 3px 4px #D6D6D6"};
  border-bottom-width: ${props => (props.noEffect ? 0 : 1)};
  border-bottom-color: #d6d6d6;
  height: 50;
  align-items: center;
  padding-horizontal: ${props =>
    props.isSearchFieldVisible || props.hasBackButton ? 0 : 20};
  width: 100%;
`;

const Header = props => (
  <StyledHeader {...props} style={styles.header}>
    {(props.isSearchFieldVisible || props.hasBackButton) && (
      <TouchableOpacity
        onPress={() => props.onBackClick()}
        style={styles.backButton}
      >
        <Icon
          name="chevron-left"
          type="MaterialCommunityIcons"
          style={styles.backButtonIcon}
        />
      </TouchableOpacity>
    )}

    {!props.isSearchFieldVisible ? (
      <Fragment>
        <Text style={styles.heading}>{props.heading.toUpperCase()}</Text>
        <View style={styles.rightBox}>
          {props.hasSearch && (
            <Icon
              name="search"
              type="FontAwesome"
              style={styles.icon}
              onPress={() => props.onSearchClick()}
            />
          )}
          {props.hasFilter && (
            <Icon name="filter" type="FontAwesome" style={styles.icon} />
          )}
          {props.hasSort && (
            <Icon name="sort" type="FontAwesome" style={styles.icon} />
          )}
          {props.hasView && (
            <Icon
              name={props.view === "list" ? "th-large" : "list-ul"}
              type="FontAwesome"
              style={styles.icon}
            />
          )}
        </View>
      </Fragment>
    ) : (
      <TextInput
        placeholder="Enter keywords"
        style={styles.searchField}
        underlineColorAndroid="transparent"
      />
    )}
  </StyledHeader>
);

export default Header;

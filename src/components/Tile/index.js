import React, { Fragment } from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { Icon } from "native-base";
import styled from "styled-components";
import Text from "../Text";
import styles from "./styles";

const StyledTile = styled.View`
  padding: 10px;
  ${props => +(props.height ? `height:${props.height}` : null)};
  width: ${props => (props.width ? props.width : "100%")};
  justify-content: center;
`;

const Tile = props => (
  <Fragment>
    {props.hide && props.value.includes("NaN") ? null : (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => props.onArrowPress(props)}
      >
        <StyledTile
          {...props}
          style={[props.style, props.isRow ? styles.isRow : null]}
        >
          {(!props.secondaryValue ||
            (!!props.secondaryValue && !props.value.includes("NaN"))) && (
            <Fragment>
              {!!props.icon && (
                <Image
                  style={[props.isRow ? styles.isRowIcon : styles.icon]}
                  source={{ uri: props.icon }}
                  resizeMode="contain"
                />
              )}
              <View style={styles.content}>
                <Text {...props.childProps}>{props.title}</Text>
                {!!props.value && (
                  <Text
                    style={styles.impText}
                    {...props.childProps}
                    size={!!props.childProps.strongText || "xl"}
                  >
                    {props.value}
                  </Text>
                )}
                {!!props.secondaryValue && (
                  <Text style={styles.secondaryValue} {...props.childProps}>
                    {" "}
                    {props.secondaryValue}
                  </Text>
                )}
              </View>
              {!!props.isArrowIcon && (
                <Icon
                  style={styles.arrowIcon}
                  type="MaterialCommunityIcons"
                  name="chevron-right"
                />
              )}
            </Fragment>
          )}
        </StyledTile>
      </TouchableOpacity>
    )}
  </Fragment>
);

export default Tile;

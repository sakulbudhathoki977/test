import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  icon: {
    marginBottom: 10,
    height: 40,
    width: 40
  },
  isRow: {
    flexDirection: "row",
    alignItems: "center"
  },
  isRowIcon: {
    marginRight: 10,
    flex: 1
  },
  impText: {
    marginTop: 5
  },
  secondaryValue: {
    marginTop: 8
  },
  content: {
    justifyContent: "center"
  },
  arrowButton: {
    position: "absolute",
    right: 0,
    alignItems: "center",
    alignSelf: "center"
  },
  arrowIcon: {
    color: "white",
    fontSize: 32,
    position: "absolute",
    right: 10
  }
});

export default styles;

import React from "react";
import { View } from "react-native";
import Text from "../Text";
import styles from "./styles";

const Status = props => (
  <View style={[styles.box, props.style, styles[props.title]]}>
    <Text style={styles.text}>{props.title}</Text>
  </View>
);

export default Status;

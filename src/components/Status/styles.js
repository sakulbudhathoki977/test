import { StyleSheet } from "react-native";
import { theme } from "../../utils";

const styles = StyleSheet.create({
  box: {
    paddingVertical: 3,
    paddingHorizontal: 14,
    borderRadius: 10,
    backgroundColor: "black"
  },
  text: {
    color: "white",
    fontSize: 12
  },
  Pending: {
    backgroundColor: theme.color.pending
  },
  Cleared: {
    backgroundColor: theme.color.cleared
  },
  Overdue: {
    backgroundColor: theme.color.overdue
  },
  Forgiven: {
    backgroundColor: theme.color.forgiven
  },
  Canceled: {
    backgroundColor: theme.color.canceled
  },
  Forgiven: {
    backgroundColor: theme.color.forgiven
  }
});

export default styles;

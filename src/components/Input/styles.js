const styles = {
  inputWrapper: {
    flex: 1,
    marginRight: 5
  },
  textInputStyle: {
    color: "#fff",
    fontSize: 14,
    margin: 10,
    height: 40,
    fontFamily: "Lato",
    borderBottomWidth: 0
  },
  singleInputWrapper: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "white"
  },
  iconWrapper: {
    textAlign: "center",
    paddingTop: 20,
    paddingLeft: 10,
    color: "white",
    fontSize: 20,
    borderBottomWidth: 0
  },
  placeholderTextColor: "white"
};

export default styles;

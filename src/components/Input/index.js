import React from "react";
import {
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { Icon } from "native-base";
import styles from "./styles";

const Input = props => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    <View style={styles.singleInputWrapper}>
      <Icon
        style={styles.iconWrapper}
        name={props.iconName}
        type={props.iconType}
      />
      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.textInputStyle}
          autoCapitalize="none"
          value={props.value}
          onChangeText={props.onInputChange}
          placeholder={props.placeholder}
          keyboardType={props.keyboardType}
          placeholderTextColor={styles.placeholderTextColor}
          autoCorrect={false}
          returnKeyType={props.returnKeyType}
          onSubmitEditing={() => Keyboard.dismiss()}
          secureTextEntry={props.secureTextEntry || false}
          underlineColorAndroid="transparent"
        />
      </View>
    </View>
  </TouchableWithoutFeedback>
);

export default Input;

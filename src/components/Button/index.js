import React from "react";

import { Icon, Button } from "native-base";
import { Text } from "./../../components";
import { TouchableOpacity } from "react-native";
const styles = {
  button: {
    flexDirection: "row",
    alginItems: "center",
    justifyContent: "center",
    shadowOffset: { height: 1, width: 1 },
    shadowOpacity: 0.2,
    elevation: 1
  }
};
const CustomButton = props => (
  <Button
    {...props}
    onPress={() => props.onPress()}
    style={[props.style, styles.button]}
  >
    {props.iconProps && <Icon {...props.iconProps} />}
    {props.title && <Text {...props.titleProps}>{props.title}</Text>}
  </Button>
);

export default CustomButton;

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  title: {
    marginTop: 10,
    fontSize: 16
  },
  icon: {
    fontSize: 40,
    color: "#BFF45E"
  },
  iconAlert: {
    fontSize: 60,
    color: "#C0CE30"
  },
  bigButton: {
    backgroundColor: "#15A0DF",
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 40,
    height: 40,
    paddingHorizontal: 20,
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    elevation: 0,
    borderRadius: 0
  }
});

export default styles;

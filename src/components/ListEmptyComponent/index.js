import React, { Fragment } from "react";
import { Icon, Button } from "native-base";
import { Linking } from "react-native";
import styled from "styled-components";
import Text from "../Text";
import styles from "./styles";

const StyledHeader = styled.View`
  align-items: center;
  justify-content: center;
  background-color: white;
  padding-vertical: 20;
  padding-horizontal: 20;
`;
const navigateToGoogle = () => {
  Linking.openURL(`http://google.com`);
};
const Header = props => (
  <StyledHeader {...props}>
    {!props.loading && (
      <Fragment>
        {props.for === "loan" && (
          <Fragment>
            <Icon
              style={styles.icon}
              type="Ionicons"
              name="information-circle"
            />
            <Text style={styles.title}>Nothing to show.</Text>
          </Fragment>
        )}

        {props.for === "notification" && (
          <Fragment>
            <Icon style={styles.iconAlert} type="FontAwesome" name="smile-o" />
            <Text style={styles.title} center>
              You have no notification.
            </Text>
            <Text style={styles.title} center>
              Enjoy your day !
            </Text>
          </Fragment>
        )}

        {props.for === "dashboard" && (
          <Fragment>
            <Text center color="#30AFE2" size="xl" fontWeight={8}>
              Welcome
            </Text>
            <Text center fontWeight={4} style={{ marginTop: 6 }}>
              {props.fullName}
            </Text>
            <Text
              color="#6F7694"
              fontWeight={6}
              style={{
                alignSelf: "flex-start",
                marginBottom: 5,
                marginTop: 15
              }}
            >
              Get Started
            </Text>
            <Text>
             Thanks for using our app. Hehe.
            </Text>
            <Button
              onPress={() => navigateToGoogle()}
              style={styles.bigButton}
            >
              <Text color="white">Go To Website</Text>
            </Button>
          </Fragment>
        )}
      </Fragment>
    )}
  </StyledHeader>
);

export default Header;

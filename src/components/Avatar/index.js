import React from "react";
import styled from "styled-components";
import Text from "../Text";

const baseStyle = `
align-self: center;
height: 70;
width: 70;
border-radius: 35;
margin-right: 20px;
justifyContent: center
`;
const StyledAvatar = styled.Image`
  ${baseStyle};
`;

const StyledView = styled.View`
  ${baseStyle};
  border-width: 5;
  border-color: white;
`;

const Avatar = props => {
  if (props.url) {
    return (
      <StyledAvatar
        {...props}
        style={[props.style]}
        source={{ uri: props.url }}
      />
    );
  }

  return (
    <StyledView>
      <Text center size="xl" color="white">
        {props.alt}
      </Text>
    </StyledView>
  );
};

export default Avatar;

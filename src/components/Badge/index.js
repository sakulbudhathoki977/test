import React from "react";

import { TouchableOpacity, Text } from "react-native";
const styles = {
  badge: {
    borderRadius: 2,
    backgroundColor: "#4DAD4A",
    borderRadius: 10,
    paddingVertical: 1,
    paddingHorizontal: 5
  },
  title: {
    color: "white",
    fontSize: 10
  }
};
const Button = props => (
  <TouchableOpacity
    onPress={() => props.onPress()}
    style={[props.style, styles.badge]}
  >
    <Text style={[props.titleStyle, styles.title]}>{props.title}</Text>
  </TouchableOpacity>
);

export default Button;

import { StyleSheet } from "react-native";
import { theme } from "../../utils";

const { fontFamily } = theme;

export default (styles = StyleSheet.create({
  initialStyle: {
    fontFamily
  },
  center: {
    alignSelf: "center"
  }
}));

export const fontSizeStyler = type => {
  let size;
  switch (type) {
    case "sm":
      size = 10;
      break;
    case "m":
      size = 12;
      break;
    case "l":
      size = 16;
      break;
    case "xl":
      size = 22;
      break;
    case "xxl":
      size = 28;
      break;
    default:
      size = 12;
  }

  return { fontSize: size };
};

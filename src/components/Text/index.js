import React from "react";
import styled from "styled-components";
import { theme } from "../../utils";

const { fontFamily, color } = theme;
const fontSizeStyler = size => {
  switch (size) {
    case "sm":
      return 10;
    case "l":
      return 16;
    case "xl":
      return 22;
    case "xxl":
      return 28;
    default:
      return 14;
  }
};

const StyledText = styled.Text`
  font-family: ${fontFamily};
  align-self: ${props =>
    props.center ? "center" : props.alignSelf ? props.alignSelf : "auto"};
  font-size: ${props => fontSizeStyler(props.size)}px;
  color: ${props => (props.color ? props.color : color.defaultTextColor)};
  font-weight: ${props => (props.fontWeight ? props.fontWeight * 100 : 100)};
`;

const Text = props => (
  <StyledText {...props} allowFontScaling={false}>
    {props.children}
  </StyledText>
);

export default Text;

import React from "react";
import { Dimensions } from "react-native";
import styled from "styled-components";

const { width } = Dimensions.get("window");

const StyledImage = styled.Image`
  width: ${props => width / props.widthRatio};
  height: ${props => width / props.widthRatio / 3.46875};
  z-index: ${props => (props.zIndex ? props.zIndex : 0)};
  bottom: 0;
  right: 0;
  position: absolute;
`;

const StylishImage = props => (
  <StyledImage {...props} style={[props.style]} source={{ uri: "style_bg" }} />
);

export default StylishImage;

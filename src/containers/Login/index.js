import React, { Component, Fragment } from "react";
import {
  View,
  Image,
  ActivityIndicator,
  Alert,
  Keyboard,
  AsyncStorage,
  Linking,
  Switch,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Button } from "native-base";
import axios from "axios";
import OneSignal from "react-native-onesignal";
import { Input, Text } from "../../components";
import {
  updateLoginField,
  loginUser,
  updateDeviceInfo,
  fetchUserDetailByToken,
  resetLogin
} from "./actions";
import styles from "./styles";
import { ONESIGNAL_APP_KEY } from "../../../env";

class Login extends Component {
  componentWillMount() {
    OneSignal.init(ONESIGNAL_APP_KEY, { kOSSettingsKeyAutoPrompt: true });
    // OneSignal.addEventListener('received', this.onReceived);
    // OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener("ids", this.onIds);
    // OneSignal.configure();

    OneSignal.getPermissionSubscriptionState(status => {
      this.props.updateLoginField("deviceID", status.userId);
    });
    try {
      AsyncStorage.getItem("loginToken").then(token => {
        if (token) {
          this.nextTaskAfterLoginSuccess(token);
        } else {
          this.props.updateLoginField("initialLoading", false);
        }
      });
    } catch (err) {
      this.props.updateLoginField("initialLoading", false);
    }
  }

  /*
  onReceived(notification) {
      console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }


  onIds = device => {
      console.log(device.userId);
  }
  */

  componentWillReceiveProps(nextProps) {
    if (nextProps.loginErr) {
      this.props.updateLoginField("loginErr", false);
      if (nextProps.loginErr.includes("Token")) {
        return false;
      }
      Alert.alert("Authentication failed !", nextProps.loginErr);
      return false;
    }
    if (nextProps.tokenSuccess) {
      this.props.updateDeviceInfo({
        client_guid: nextProps.clientDetails.details[0].guid,
        device_id: this.props.deviceID,
        device_type: Platform.OS,
        is_active: 1
      });
      this.props.resetLogin();
      this.props.updateLoginField("initialLoading", false);
      this.props.navigation.navigate("Dashboard");
      return false;
    }

    if (nextProps.loginSuccess && nextProps.detail.loginToken) {
      if (this.props.rememberMe) {
        AsyncStorage.setItem("loginToken", nextProps.detail.loginToken).then(
          () => {
            this.nextTaskAfterLoginSuccess(nextProps.detail.loginToken);
          }
        );
      } else {
        this.nextTaskAfterLoginSuccess(nextProps.detail.loginToken);
      }
    }
    return true;
  }

  componentWillUnmount() {
    //  OneSignal.removeEventListener('received', this.onReceived);
    //    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener("ids", this.onIds);
  }

  redirect = link => {
    Linking.openURL(link).catch(err => console.error("An error occurred", err));
  };

  onLoginPress = () => {
    Keyboard.dismiss();

    if (this.props.email == "" || this.props.password == "") {
      Alert.alert("Missing data !", "All fields are mandatory");
    } else {
      this.props.loginUser({
        user_name: this.props.email,
        pwd: this.props.password
      });
    }
  };

  nextTaskAfterLoginSuccess = token => {
    axios.defaults.headers.common.token = token;
    this.props.fetchUserDetailByToken();
  };

  showButtonOrSpinner() {
    if (this.props.loginLoading || this.props.tokenLoading) {
      return <ActivityIndicator size="small" color="#60AA00" />;
    }
    return <Text style={styles.loginButtonText}>LOGIN</Text>;
  }

  render() {
    return (
      <View
        style={[
          this.props.initialLoading
            ? styles.loginWrapperInitial
            : styles.loginWrapper
        ]}
      >
        <Image
          source={{ uri: "logo" }}
          style={styles.logo}
          resizeMode="contain"
        />
        <Image
          source={{ uri: "login" }}
          style={styles.background}
          resizeMode="cover"
        />

        {!this.props.initialLoading ? (
          <Fragment>
            <Input
              iconName="email"
              iconType="MaterialIcons"
              placeholder="enter your email"
              value={this.props.email}
              returnKeyType="next"
              keyboardType="email-address"
              type="login"
              onInputChange={value =>
                this.props.updateLoginField("email", value)
              }
            />

            <Input
              iconName="lock"
              iconType="MaterialIcons"
              placeholder="enter your password"
              secureTextEntry
              value={this.props.password}
              returnKeyType="done"
              type="login"
              onInputChange={value =>
                this.props.updateLoginField("password", value)
              }
            />
      
              <View style={styles.checkContainer}>
                <Text color="white">Remember Me</Text>
                <Switch
                  style={styles.checkBox}
                  value={this.props.rememberMe}
                  onValueChange={() =>
                    this.props.updateLoginField(
                      "rememberMe",
                      !this.props.rememberMe
                    )
                  }
                />
              </View>
            <Button
              onPress={() => this.onLoginPress()}
              light
              full
              style={styles.loginButton}
            >
              {this.showButtonOrSpinner()}
            </Button>

             <View style={styles.signUp}>
                <Text style={styles.registerInfo}>Don't have an account? </Text>
                <Text
                  onPress={() =>
                    this.redirect("www.google.com")
                  }
                  style={[styles.registerInfoLink]}
                >Sign up
                </Text>
              </View>
           
            <View style={styles.linkToRegister} />
            <Text
              onPress={() =>
                this.redirect("www.google.com")
              }
              style={styles.termsCondition}
            >
              Terms & Conditions
            </Text>
          </Fragment>
        ) : (
          <ActivityIndicator color="#C0D631" />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  email: state.login.email,
  password: state.login.password,
  detail: state.login.detail,
  loginErr: state.login.loginErr,
  loginSuccess: state.login.loginSuccess,
  loginLoading: state.login.loginLoading,
  clientDetails: state.login.clientDetails,
  tokenErr: state.login.tokenErr,
  tokenSuccess: state.login.tokenSuccess,
  tokenLoading: state.login.tokenLoading,
  rememberMe: state.login.rememberMe,
  initialLoading: state.login.initialLoading,
  deviceID: state.login.deviceID
});

const mapDispatchToProps = dispatch => ({
  updateLoginField: (...args) => dispatch(updateLoginField(...args)),
  loginUser: fields => dispatch(loginUser(fields)),
  updateDeviceInfo: deviceInfo => dispatch(updateDeviceInfo(deviceInfo)),
  fetchUserDetailByToken: token => dispatch(fetchUserDetailByToken(token)),
  resetLogin: () => dispatch(resetLogin())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

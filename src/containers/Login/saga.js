import { call, put, takeLatest } from "redux-saga/effects";
import {
  USER_LOGIN,
  USER_DEVICE_UPDATE,
  FETCH_USER_DETAIL_BY_TOKEN
} from "./constants";
import {
  loginUserSuccess,
  loginUserFailed,
  fetchUserDetailByTokenSuccess,
  fetchUserDetailByTokenFailed
} from "./actions";
import { request } from "../../utils";

export function* loginUser(action) {
  const api = "/login";
  try {
    const user = yield call(request, "post", api, action.fields);
    yield put(loginUserSuccess(user));
  } catch (err) {
    yield put(loginUserFailed(err.message));
  }
}

export function* updateDeviceInfo(action) {
  const api = "/client/device/store";
  try {
    const storeDevice = yield call(request, "post", api, action.deviceInfo);
  } catch (err) {}
}

export function* fetchUserDetailByToken() {
  const api = "/client/details/get";
  try {
    const res = yield call(request, "post", api, {});
    yield put(fetchUserDetailByTokenSuccess(res.clientDetails));
  } catch (err) {
    yield put(fetchUserDetailByTokenFailed(err.message));
  }
}

export default function* loginSaga() {
  yield takeLatest(USER_LOGIN, loginUser);
  yield takeLatest(USER_DEVICE_UPDATE, updateDeviceInfo);
  yield takeLatest(FETCH_USER_DETAIL_BY_TOKEN, fetchUserDetailByToken);
}

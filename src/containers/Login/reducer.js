import {
  RESET_LOGIN,
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  UPDATE_LOGIN_FIELD,
  FETCH_USER_DETAIL_BY_TOKEN,
  FETCH_USER_DETAIL_BY_TOKEN_SUCCESS,
  FETCH_USER_DETAIL_BY_TOKEN_FAILED
} from "./constants";

const initialState = {
  email: "",
  password: "",
  initialLoading: true,
  loginErr: false,
  loginLoading: false,
  loginSuccess: false,
  tokenErr: false,
  tokenLoading: false,
  tokenSuccess: false,
  detail: {},
  clientDetails: {},
  rememberMe: false,
  deviceID: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case RESET_LOGIN:
      return {
        ...initialState,
        deviceID: state.deviceID,
        detail: state.detail,
        clientDetails: state.clientDetails
      };

    case UPDATE_LOGIN_FIELD:
      return {
        ...state,
        [action.key]: action.value,
        err: null
      };

    case USER_LOGIN:
      return {
        ...state,
        loginLoading: true,
        loginErr: false,
        loginSuccess: false,
        detail: null
      };

    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        loginLoading: false,
        tokenLoading: true,
        loginErr: false,
        loginSuccess: true,
        detail: action.detail
      };

    case USER_LOGIN_FAILED:
      return {
        ...state,
        loginLoading: false,
        tokenLoading: false,
        loginErr: action.err,
        loginSuccess: false,
        detail: null,
        initialLoading: false
      };

    case FETCH_USER_DETAIL_BY_TOKEN:
      return {
        ...state,
        tokenLoading: true,
        tokenErr: false,
        tokenSuccess: false,
        clientDetails: null
      };

    case FETCH_USER_DETAIL_BY_TOKEN_SUCCESS:
      return {
        ...state,
        tokenLoading: false,
        tokenErr: false,
        tokenSuccess: true,
        password: "",
        clientDetails: action.clientDetails
      };

    case FETCH_USER_DETAIL_BY_TOKEN_FAILED:
      return {
        ...state,
        tokenLoading: false,
        loginLoading: false,
        tokenErr: action.err,
        tokenSuccess: false,
        clientDetails: null,
        initialLoading: false
      };
    default:
      return state;
  }
}

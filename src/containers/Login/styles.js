import { StyleSheet, Dimensions } from "react-native";
import { theme } from "../../utils";

const { width, height } = Dimensions.get("window");
const { fontFamily } = theme;

export default StyleSheet.create({
  header: {
    backgroundColor: "transparent",
    height: 0
  },
  linearGradient: {
    flex: 1
  },
  loginWrapper: {
    flex: 1,
    padding: 30
  },
  loginWrapperInitial: {
    flex: 1,
    justifyContent: "center"
  },
  padding: {
    paddingHorizontal: 5
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  signUp: {
    top: 12,
    position: "absolute",
    right: 5
  },

  registerInfo: {
    color: "#fff",
    fontFamily,
    alignSelf: "center"
  },
  registerInfoLink: {
    color: "#fff",
    fontFamily,
    textDecorationLine: "underline"
  },
  termsCondition: {
    color: "#fff",
    fontFamily,
    textDecorationLine: "underline",
    alignSelf: "center",
    position: "absolute",
    bottom: 20,
    zIndex: 99
  },
  linkToRegister: {
    flexDirection: "row",
    alignSelf: "center",
    marginTop: 20,
    justifyContent: "center"
  },
  logo: {
    alignSelf: "center",
    width: "70%",
    height: 120
  },
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    width,
    height,
    zIndex: -1
  },
  loginButton: {
    marginTop: 30,
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    elevation: 0
  },
  loginButtonText: {
    color: '#282C34',
    fontWeight: "bold",
    fontFamily: theme.fontFamily
  },
  registrationHeader: {
    paddingRight: 10
  },
  loginBtnTextStyle: {
    fontFamily,
    fontWeight: "bold",
    textAlign: "center",
    color: theme.primaryColor
  },
  styleBg: {
    position: "absolute",
    right: 0,
    bottom: 0,
    width: width / 1.2,
    height: width / 1.2 / 3.46875
  },
  checkContainer: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  checkBox: {
    marginLeft: 8,
    backgroundColor: "#72C100",
    alignItems: "center",
    justifyContent: "center"
  }
});

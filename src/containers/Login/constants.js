export const UPDATE_LOGIN_FIELD = "test/Login/UPDATE_LOGIN_FIELD";
export const RESET_LOGIN = "test/Login/RESET_LOGIN";

export const USER_LOGIN = "test/Login/USER_LOGIN";
export const USER_LOGIN_SUCCESS = "test/Login/USER_LOGIN_SUCCESS";
export const USER_LOGIN_FAILED = "test/Login/USER_LOGIN_FAILED";

export const FETCH_USER_DETAIL_BY_TOKEN =
  "test/Login/FETCH_USER_DETAIL_BY_TOKEN";
export const FETCH_USER_DETAIL_BY_TOKEN_SUCCESS =
  "test/Login/FETCH_USER_DETAIL_BY_TOKEN_SUCCESS";
export const FETCH_USER_DETAIL_BY_TOKEN_FAILED =
  "test/Login/USER_LOGIN_FAILED";

export const USER_DEVICE_UPDATE = "test/Login/USER_DEVICE_UPDATE";

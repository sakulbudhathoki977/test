import {
  UPDATE_LOGIN_FIELD,
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_DEVICE_UPDATE,
  FETCH_USER_DETAIL_BY_TOKEN,
  FETCH_USER_DETAIL_BY_TOKEN_SUCCESS,
  FETCH_USER_DETAIL_BY_TOKEN_FAILED,
  RESET_LOGIN
} from "./constants";

export const resetLogin = () => ({
  type: RESET_LOGIN
});

export const updateLoginField = (key, value) => ({
  type: UPDATE_LOGIN_FIELD,
  key,
  value
});

export const loginUser = fields => ({
  type: USER_LOGIN,
  fields
});

export const loginUserSuccess = detail => ({
  type: USER_LOGIN_SUCCESS,
  detail
});

export const loginUserFailed = err => ({
  type: USER_LOGIN_FAILED,
  err
});

export const updateDeviceInfo = deviceInfo => ({
  type: USER_DEVICE_UPDATE,
  deviceInfo
});

export const fetchUserDetailByToken = () => ({
  type: FETCH_USER_DETAIL_BY_TOKEN
});

export const fetchUserDetailByTokenSuccess = clientDetails => ({
  type: FETCH_USER_DETAIL_BY_TOKEN_SUCCESS,
  clientDetails
});

export const fetchUserDetailByTokenFailed = err => ({
  type: FETCH_USER_DETAIL_BY_TOKEN_FAILED,
  err
});

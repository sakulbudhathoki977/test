import {
  USER_LOGOUT,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILED,
  UPDATE_SETTINGS_FORM_FIELD
} from "./constants";

export const logoutUser = () => ({
  type: USER_LOGOUT
});

export const logoutUserSuccess = () => ({
  type: USER_LOGOUT_SUCCESS
});

export const logoutUserFailed = err => ({
  type: USER_LOGOUT_FAILED,
  err
});

export const updateFormField = (key, value) => ({
  type: UPDATE_SETTINGS_FORM_FIELD,
  key,
  value
});

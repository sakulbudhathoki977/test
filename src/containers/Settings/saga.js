import { call, put, takeLatest } from "redux-saga/effects";
import { logoutUserSuccess, logoutUserFailed } from "./actions";
import { USER_LOGOUT } from "./constants";
import { request } from "../../utils";

export function* logoutUser() {
  const api = "/logout";
  try {
    yield call(request, "post", api);
    yield put(logoutUserSuccess());
  } catch (err) {
    yield put(logoutUserFailed(err));
  }
}

export default function* settingsSaga() {
  yield takeLatest(USER_LOGOUT, logoutUser);
}

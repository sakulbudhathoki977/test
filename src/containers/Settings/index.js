import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  View,
  FlatList,
  Linking,
  ActivityIndicator,
  AsyncStorage,
  ScrollView
} from "react-native";
import {
  Text,
  Header,
  StylishImage,
  Avatar,
  ListItem,
  Button
} from "../../components";
import { theme, helpers } from "../../utils";

import styles from "./styles";
import { logoutUser, updateFormField } from "./actions";
import { updateLoginField } from "../Login/actions";

const listArray = [
  {
    heading: "Account",
    data: [
      {
        id: 0,
        title: "Profile",
        icon: "bell",
        iconType: "MaterialCommunityIcons",
        navigate: "Profile"
      },
      {
        id: 1,
        title: "Wallet",
        icon: "help",
        iconType: "MaterialCommunityIcons",
        navigate: "Wallet"
      },
      {
        id: 2,
        title: "Theme",
        icon: "lock",
        iconType: "MaterialCommunityIcons",
        navigate: "Theme"
      }
    ]
  },
  {
    heading: "Helps",
    data: [
      {
        id: 0,
        title: "FAQ",
        icon: "bell",
        iconType: "MaterialCommunityIcons",
        navigate: "Profile"
      },
      {
        id: 1,
        title: "Privacy Policy",
        icon: "help",
        iconType: "MaterialCommunityIcons",
        navigate: "Wallet"
      },
      {
        id: 2,
        title: "Give Feedback",
        icon: "lock",
        iconType: "MaterialCommunityIcons",
        redirect: "Theme"
      }
    ]
  }
];

class Settings extends Component {
  async componentWillReceiveProps(props) {
    if (props.logoutSuccess) {
      await AsyncStorage.removeItem("loginToken");
      this.props.navigation.navigate("Login");
    }
  }

  navigateOrRedirect = item => {
    console.log(item);
    if (item.navigate) {
      this.props.navigation.navigate(item.navigate);
    } else if (item.redirect) {
      Linking.openURL(item.redirect).catch(() => {});
    }
  };

  showButtonOrSpinner() {
    // if (!this.props.logoutLoading) {
    return <Text style={styles.logoutText}>LOG OUT</Text>;
    // }
    // return <ActivityIndicator size="small" color="#455CF6" />;
  }

  render() {
    const { avatarUrl, firstName, middleName, lastName, email } = this.props;
    const fullName =
      firstName + (middleName ? ` ${middleName} ` : " ") + lastName;
    const listProps = {
      isIcon: true,
      isArrowIcon: true,
      style: {
        height: 68
      }
    };
    return (
      <Fragment>
        <View style={styles.header}>
          <Avatar url={avatarUrl} alt={helpers.getInitial(fullName)} />
          <View>
            <Text style={styles.name}>{fullName}</Text>
            <Text style={styles.email}>{email}</Text>
          </View>
        </View>
        <ScrollView>
          {listArray.map(list => (
            <Fragment>
              <Text style={styles.listHeading}>{list.heading}</Text>
              <FlatList
                data={list.data}
                style={theme.flatList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <ListItem
                    {...listProps}
                    onListPress={listItem => this.navigateOrRedirect(listItem)}
                    {...item}
                  />
                )}
              />
            </Fragment>
          ))}
          <Button full style={styles.logoutButton} title="LOG OUT" />
        </ScrollView>
      </Fragment>
    );
  }
}

const select = state => ({
  firstName: "Sakul",
  middleName: "",
  lastName: "Budhathoki",
  email: "sakulbudhathoki977@gmail.com",
  avatarUrl: "https://avatars0.githubusercontent.com/u/13379067?s=400&v=4",
  logoutLoading: state.settings.logoutLoading,
  logoutSuccess: state.settings.logoutSuccess,
  logoutErr: state.settings.logoutErr
});

const actions = dispatch => ({
  updateFormField: (...args) => dispatch(updateFormField(...args)),
  logoutUser: () => dispatch(logoutUser()),
  updateLoginField: (...args) => dispatch(updateLoginField(...args))
});

export default connect(
  select,
  actions
)(Settings);

// <Header heading="Settings" noEffect />

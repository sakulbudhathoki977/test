import { StyleSheet } from "react-native";

export default StyleSheet.create({
  header: {
    paddingVertical: 20,
    paddingHorizontal: 30,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    borderBottomColor: "#E8E8E8",
    borderBottomWidth: 1
  },
  title: {
    color: "black",
    fontSize: 18,
    fontWeight: "700"
  },
  name: {
    color: "black",
    fontSize: 14
  },
  email: {
    color: "black",
    fontSize: 12
  },
  logoutButton: {
    marginTop: 10,
    backgroundColor: "white",
    paddingHorizontal: 25,
    borderRadius: 40,
    margin: 20
  },
  logputButtonText: {
    color: "#1E1E1E",
    fontWeight: "600"
  },
  listHeading: {
    marginLeft: 20,
    marginBottom: 10,
    marginTop: 20,
    fontWeight: "700",
    color: "#1E1E1E"
  }
});

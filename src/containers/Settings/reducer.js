import {
  USER_LOGOUT,
  USER_LOGOUT_FAILED,
  USER_LOGOUT_SUCCESS,
  UPDATE_SETTINGS_FORM_FIELD
} from "./constants";

const initialState = {
  logoutLoading: false,
  logoutSuccess: false,
  logoutErr: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case USER_LOGOUT:
      return {
        ...state,
        logoutLoading: true
      };
    case USER_LOGOUT_SUCCESS:
      return {
        ...state,
        logoutSuccess: true,
        logoutLoading: false
      };
    case USER_LOGOUT_FAILED:
      return {
        ...state,
        logoutLoading: false
      };
    case UPDATE_SETTINGS_FORM_FIELD:
      return {
        ...state,
        [action.key]: action.value
      };

    default:
      return state;
  }
}

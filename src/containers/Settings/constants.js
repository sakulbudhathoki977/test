export const UPDATE_SETTINGS_FORM_FIELD =
  "test/Settings/UPDATE_SETTINGS_FORM_FIELD";

export const USER_LOGOUT = "test/Settings/USER_LOGOUT";
export const USER_LOGOUT_SUCCESS = "test/Settings/USER_LOGOUT_SUCCESS";
export const USER_LOGOUT_FAILED = "test/Settings/USER_LOGOUT_FAILED";

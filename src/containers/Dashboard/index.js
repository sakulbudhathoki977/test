import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  View,
  FlatList,
  RefreshControl,
  Dimensions,
  Platform,
  ScrollView,
  ActivityIndicator,
  Image,
  TextInput
} from "react-native";
import { Container } from "native-base";
import { NavigationActions } from "react-navigation";
import moment from "moment";
import { fetchNotification } from "../Notification/actions";
import {
  Header,
  Tile,
  ListItem,
  ListEmptyComponent,
  Text
} from "../../components";
import { helpers, theme } from "../../utils";
import { fetchDashboardData, updateDashboardField } from "./actions";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

class Dashboard extends Component {
  componentWillMount() {
    this.props.navigation.addListener("willFocus", () => {
      // this.preFetch();
    });
  }

  componentDidUpdate(props) {
    // if (props.unreadCount !== this.props.unreadCount) {
    //   const setParamsAction = NavigationActions.setParams({
    //     params: {unreadCount: this.props.unreadCount},
    //     key: 'Alerts'
    //   });
    //   this.props.navigation.dispatch(setParamsAction);
    // }
  }

  onListPress = arg => {
    this.props.navigation.navigate("TestPage", {
      luid: arg.guid,
      data: arg
    });
  };

  handleTilePress = arg => {
    if (arg.id === 2 && this.props.data.next_time.display_guid) {
      this.props.navigation.navigate("display", {
        displayGuid: this.props.data.next_time.display_guid
      });
    }
  };

  preFetch() {
    this.props.fetchNotification();
    this.props.fetchDashboardData();
  }

  render() {
    const backgroundImage =
      "http://www.futsalnimes.com/wp-content/uploads/2016/11/648.jpg";
    return (
      <Container style={styles.viewContainer}>
        <View style={styles.cover}>
          <Image
            style={styles.backgroundImage}
            source={{ uri: backgroundImage }}
          />
          <TextInput
            style={styles.search}
            placeholder="Search anything you want"
            underlineColorAndroid="transparent"
            placeholderTextColor="#929292"
          />
        </View>
      </Container>
    );
  }
}

const selector = state => ({
});

const actions = dispatch => ({
  fetchNotification: () => dispatch(fetchNotification()),
  fetchDashboardData: () => dispatch(fetchDashboardData()),
  updateDashboardField: (...args) => dispatch(updateDashboardField(...args))
});

export default connect(
  selector,
  actions
)(Dashboard);

import { StyleSheet, Dimensions, Platform } from "react-native";
import { theme } from "../../utils";

const { height } = Dimensions.get("window");

const styles = StyleSheet.create({
  backgroundImage: {
    height: 300,
    width: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: -1
  },
  search: {
    marginTop: 40,
    marginHorizontal: 16,
    borderRadius: 20,
    height: 40,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    fontFamily: "Abel"
  }
});

export default styles;

import { call, put, takeLatest } from "redux-saga/effects";
import { FETCH_DASHBOARD_DATA } from "./constants";
import { fetchDashboardDataSuccess, fetchDashboardDataFailed } from "./actions";
import { request } from "../../utils";

export function* fetchDashboardData() {
  const api = "/get-dashboard-stats";

  try {
    const dashboardData = yield call(request, "post", api, {});
    yield put(fetchDashboardDataSuccess(dashboardData.result));
  } catch (err) {
    yield put(fetchDashboardDataFailed());
  }
}

export default function* dashboardSaga() {
  yield takeLatest(FETCH_DASHBOARD_DATA, fetchDashboardData);
}

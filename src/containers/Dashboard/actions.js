import {
  FETCH_DASHBOARD_DATA,
  FETCH_DASHBOARD_DATA_SUCCESS,
  FETCH_DASHBOARD_DATA_FAIL,
  UPDATE_DASHBOARD_FIELD
} from "./constants";

export const updateDashboardField = (key, value) => ({
  type: UPDATE_DASHBOARD_FIELD,
  key,
  value
});

export const fetchDashboardData = () => ({
  type: FETCH_DASHBOARD_DATA
});

export const fetchDashboardDataSuccess = data => ({
  type: FETCH_DASHBOARD_DATA_SUCCESS,
  data
});

export const fetchDashboardDataFailed = err => ({
  type: FETCH_DASHBOARD_DATA_FAIL,
  err
});

import {
  FETCH_DASHBOARD_DATA,
  FETCH_DASHBOARD_DATA_SUCCESS,
  FETCH_DASHBOARD_DATA_FAIL,
  UPDATE_DASHBOARD_FIELD
} from "./constants";

export const initialState = {
  loading: true,
  firstLoad: true,
  data: {
    total_value: 0,
    total_value2: 0,
    net_position: 0
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_DASHBOARD_FIELD:
      return {
        ...state,
        [action.key]: action.value
      };

    case FETCH_DASHBOARD_DATA:
      return {
        ...state,
        loading: true
      };

    case FETCH_DASHBOARD_DATA_SUCCESS:
      return {
        ...state,
        data: { ...state.data, ...action.data },
        firstLoad: false,
        loading: false
      };

    case FETCH_DASHBOARD_DATA_FAIL:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}

export const FETCH_NOTIFICATION = "test/Notification/FETCH_NOTIFICATION";
export const FETCH_NOTIFICATION_SUCCESS =
  "test/Notification/FETCH_NOTIFICATION_SUCCESS";
export const FETCH_NOTIFICATION_FAIL =
  "test/Notification/FETCH_NOTIFICATION_FAIL";

export const MARK_AS_READ = "test/Notification/MARK_AS_READ";
export const MARK_AS_READ_SUCCESS = "test/Notification/MARK_AS_READ_SUCCESS";
export const MARK_AS_READ_FAIL = "test/Notification/MARK_AS_READ_FAIL";

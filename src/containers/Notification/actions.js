import {
  MARK_AS_READ,
  MARK_AS_READ_SUCCESS,
  MARK_AS_READ_FAIL,
  FETCH_NOTIFICATION,
  FETCH_NOTIFICATION_SUCCESS,
  FETCH_NOTIFICATION_FAIL
} from "./constants";

export const markAsRead = alertGuid => ({
  type: MARK_AS_READ,
  alertGuid
});

export const markAsReadSuccess = () => ({
  type: MARK_AS_READ_SUCCESS
});

export const markAsReadFailed = err => ({
  type: MARK_AS_READ_FAIL,
  err
});

export const fetchNotification = bool => ({
  type: FETCH_NOTIFICATION,
  ptr: bool
});

export const fetchNotificationSuccess = notifications => ({
  type: FETCH_NOTIFICATION_SUCCESS,
  notifications
});

export const fetchNotificationFailed = () => ({
  type: FETCH_NOTIFICATION_FAIL
});

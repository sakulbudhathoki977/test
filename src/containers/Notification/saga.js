import { call, put, takeLatest } from "redux-saga/effects";
import { MARK_AS_READ, FETCH_NOTIFICATION } from "./constants";
import { request } from "../../utils";
import {
  fetchNotificationSuccess,
  fetchNotificationFailed,
  markAsReadSuccess,
  markAsReadFailed
} from "./actions";

export function* markAsRead(action) {
  const alertGuid = action.alertGuid;

  const reqParams = {
    alertGuid
  };
  const api = "/notifications/mark";

  try {
    yield call(request, "post", api, reqParams);
    yield put(markAsReadSuccess());
  } catch (err) {
    yield put(markAsReadFailed());
  }
}

export function* fetchNotification() {
  const api = "/notifications";
  try {
    const response = yield call(request, "post", api);
    let alerts;
    if (response.alerts) {
      alerts = response.alerts.filter(alert =>
        alert.link.includes("remove")
      );
    }

    yield put(fetchNotificationSuccess(alerts));
  } catch (err) {
    yield put(fetchNotificationFailed());
  }
}

export default function* notificationSaga() {
  yield takeLatest(MARK_AS_READ, markAsRead);
  yield takeLatest(FETCH_NOTIFICATION, fetchNotification);
}

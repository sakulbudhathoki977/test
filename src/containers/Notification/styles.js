import { StyleSheet } from "react-native";

export default StyleSheet.create({
  badge: {
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    right: -18,
    borderRadius: 10,
    height: 20,
    width: 20
  },
  badgeText: {
    color: "white",
    fontSize: 12
  }
});

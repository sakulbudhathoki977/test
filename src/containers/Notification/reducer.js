import {
  FETCH_NOTIFICATION,
  FETCH_NOTIFICATION_SUCCESS,
  FETCH_NOTIFICATION_FAIL,
  MARK_AS_READ_SUCCESS
} from "./constants";

const initialState = {
  ptr: true,
  notifications: [],
  reload: false,
  unreadCount: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_NOTIFICATION:
      return {
        ...state,
        loading: true,
        ptr: action.ptr
      };
    case FETCH_NOTIFICATION_SUCCESS:
      return {
        ...state,
        notifications: action.notifications,
        unreadCount: action.notifications.filter(n => n.read == "0").length,
        loading: false,
        reload: false,
        ptr: false
      };
    case FETCH_NOTIFICATION_FAIL:
      return {
        ...state,
        ptr: false,
        loading: false
      };
    case MARK_AS_READ_SUCCESS:
      return {
        ...state,
        reload: true
      };

    default:
      return state;
  }
}

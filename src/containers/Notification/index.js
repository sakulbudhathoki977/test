import React, { Component } from "react";
import {
  FlatList,
  RefreshControl,
  View,
  Dimensions,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Container, Icon } from "native-base";
import moment from "moment-timezone";

import { Text, Header, ListItem, ListEmptyComponent } from "../../components";
import { theme } from "../../utils";
import { markAsRead, fetchNotification } from "./actions";
import styles from "./styles";

const { height } = Dimensions.get("window");
const IconWithBadge = props => (
  <View>
    {props.count > 0 && (
      <View style={styles.badge}>
        <Text style={styles.badgeText}>{props.count}</Text>
      </View>
    )}
    <Icon
      name="bell"
      type="MaterialCommunityIcons"
      style={{ color: props.color, fontSize: 20 }}
    />
  </View>
);
class Notification extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarIcon: ({ tintColor }) => (
      <IconWithBadge
        color={tintColor}
        count={navigation.state.params && navigation.state.params.unreadCount}
      />
    )
  });

  componentWillMount() {
    this.props.navigation.addListener("willFocus", () => {
      this.props.fetchNotification(false);
    });
  }

  componentWillReceiveProps(props) {
    if (props.reloadNotification) {
      this.props.fetchNotification(false);
    }
  }

  componentDidUpdate(props) {
    if (props.unreadCount !== this.props.unreadCount) {
      this.props.navigation.setParams({ unreadCount: this.props.unreadCount });
    }
  }

  onListPress = ({ alertGuid, displayGuid }) => {
    this.props.markAsRead(alertGuid);
    this.props.navigation.navigate("display", { displayGuid });
  };

  render() {
    const notifications = this.props.notifications.map(notification => ({
      ...notification,
      primaryText: notification.alert,
      secondaryText: notification.created_text
        ? notification.created_text
        : moment(notification.created).fromNow()
    }));
    const listProps = {
      isIcon: "notification",
      listPress: true,
      primaryNoOfLines: 2,
      negationWidth: 90,
      style: {
        height: 90
      }
    };

    return (
      <Container>
        <Header heading="Alerts" />
        <FlatList
          style={theme.flatList}
          refreshControl={
            <RefreshControl
              enabled
              refreshing={!!this.props.ptr}
              onRefresh={() => this.props.fetchNotification(true)}
            />
          }
          ListEmptyComponent={
            <ListEmptyComponent
              style={styles.emptyComponent}
              loading={this.props.ptr}
              for="notification"
              height={height - 80 - (Platform.OS === "ios" ? 70 : 60)}
            />
          }
          keyExtractor={(item, index) => index.toString()}
          data={notifications}
          renderItem={({ item }) => (
            <ListItem
              {...listProps}
              onListPress={arg => this.onListPress(arg)}
              {...item}
            />
          )}
        />
      </Container>
    );
  }
}

const select = state => ({
  notifications: state.notification.notifications,
  loading: state.notification.loading,
  reloadNotification: state.notification.reload,
  unreadCount: state.notification.unreadCount,
  firstLoad: state.notification.firstLoad,
  ptr: state.notification.ptr
});

const actions = dispatch => ({
  markAsRead: alertGuid => dispatch(markAsRead(alertGuid)),
  fetchNotification: bool => dispatch(fetchNotification(bool))
});

export default connect(
  select,
  actions
)(Notification);

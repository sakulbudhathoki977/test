const degToRad = degrees => degrees * (Math.PI / 180);

const valToDeg = (val, total) =>
  ((parseFloat(val) / parseFloat(total)) * 360).toFixed();

const toPercentage = (val, total) =>
  ((parseFloat(val) / parseFloat(total)) * 100).toFixed(2);

const displayProgressCalculator = (balance, total) =>
  parseFloat(
    parseFloat(
      ((parseFloat(total) - parseFloat(balance)) / parseFloat(total)).toFixed(2)
    )
  );

const getInitial = fullName =>
  fullName
    .split(" ")
    .map(n => n[0].toUpperCase())
    .join(" ");

const removeTrailingZero = number =>
  parseFloat(
    parseFloat(number)
      .toFixed(2)
      .replace(/([0-9]+(\.[0-9]+[1-9])?)(\.?0+$)/, "$1")
  );

const formatAmountInternational = number =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

const numberAbbreviation = (num, fixed = 2) => {
  // c = `${a}`.length;
  // d = Math.pow;
  // b = d(10, b);
  // return `${(((a * b) / d(10, (c -= c % 3)) + 0.5) | 0) / b} ${
  //   "  MGTPE"[c / 3]
  // }`;

  if (num === null) {
    return null;
  } // terminate early
  if (num === 0) {
    return "0";
  } // terminate early
  fixed = !fixed || fixed < 0 ? 0 : fixed; // number of decimal places to show
  const b = num.toPrecision(2).split("e");
  // get power

  const k = b.length === 1 ? 0 : Math.floor(Math.min(b[1].slice(1), 14) / 3);
  // floor at decimals, ceiling at trillions

  const c =
    k < 1
      ? num.toFixed(0 + fixed)
      : (num / Math.pow(10, k * 3)).toFixed(1 + fixed);
  // divide by power

  const d = c < 0 ? c : Math.abs(c);
  // enforce -0 is 0

  const e = `${d} ${["", "K", "M", "B", "T"][k]}`; // append power
  return e;
};

const defaultAmountFormat = (amount, abb, abbStart = "M") => {
  if (abb && abbStart == "K" && amount >= 1000) {
    return numberAbbreviation(amount);
  }

  if (abb && abbStart == "M" && amount >= 1000000) {
    return numberAbbreviation(amount);
  }
  return formatAmountInternational(removeTrailingZero(amount));
};

const hexToRgba = (hex, opacity) => {
  let c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split("");
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = `0x${c.join("")}`;
    return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(
      ","
    )},${opacity})`;
  }
  throw new Error("Bad Hex");
};

export default {
  degToRad,
  valToDeg,
  toPercentage,
  displayProgressCalculator,
  getInitial,
  hexToRgba,
  removeTrailingZero,
  formatAmountInternational,
  defaultAmountFormat
};

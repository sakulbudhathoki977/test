import axios from "axios";
import { API_URL, xRay } from "../../env";

axios.defaults.headers.common.xRay = xRay;

const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    if (response.data && response.data.status === "success") {
      if (response.data) {
        return response.data;
      }
      return response;
    }
    if (response.data.status === "error") {
      throw new Error(response.data.message);
    } else {
      return response.data;
    }
  }
  // throw new Error(response.statusText);
};

export default (request = async (method, api, params) =>
  axios[method](API_URL + api, params).then(checkStatus));
// }
// else {
//     const error = new Error("Network Error");
//     error.response = response;
//     throw error;
// }

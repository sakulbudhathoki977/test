const vars = {
  activeTintColor: "white",
  inactiveTintColor: "#C1CFBD",
  settingsListIconColor: "#C0CE2F",
  settingsListIconArrowColor: "#DBDADA",

  placeholderStyle: {
    fontFamily: this.fontFamily
  },
  fontFamily: "Abel",
  color: {
    defaultTextColor: "#707070",
    primary: "#007FFB",
    secondary: "#703ECB",
    dark: "#010102",
    light: "#AAAABA",
    danger: "#D54E4F",
    warn: "#EDAB55",
    success: "#64B962",
    info: "#62C3DE",
    link: "#6CA7EB",
    bg: "white",
    bgPrimary: "#E7EAED"
  },
  fontSize: {
    h1: "",
    h2: "",
    h3: "",
    h4: "",
    h5: "",
    h6: ""
  },
  layout: {
    column: 4,
    margin: 16,
    gutter: 16,
    appBar: 56,
    listItem: 88,
    statusBar: 24, // Android status bar
    minTouchHeight: 48
  }
  // >720
  // layout: {
  //   column: 8,
  //   margin: 24,
  //   gutter: 24,
  // },
};
const styles = {
  defaultBorderBottom: {
    borderBottomColor: "#E8E8E8",
    borderBottomWidth: 1
  },
  defaultTextColor: { color: vars.color.defaultTextColor }
};
const theme = { ...vars, ...styles };
export default theme;

import { combineReducers } from "redux";

import dashboard from "./containers/Dashboard/reducer";
import login from "./containers/Login/reducer";
import settings from "./containers/Settings/reducer";
import notification from "./containers/Notification/reducer";

const rootReducer = {
  dashboard,
  login,
  settings,
  notification,
};

export default function createReducer(injectedReducer) {
  return combineReducers({
    ...rootReducer,
    ...injectedReducer
  });
}

import { all, fork } from "redux-saga/effects";

import dashboard from "./containers/Dashboard/saga";
import login from "./containers/Login/saga";
import settings from "./containers/Settings/saga";
import notification from "./containers/Notification/saga";

const rootSaga = function*() {
  yield all([
    fork(dashboard),
    fork(login),
    fork(settings),
    fork(notification),
  ]);
};

export default rootSaga;
